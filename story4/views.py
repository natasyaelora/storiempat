from django.shortcuts import render
from datetime import datetime, date

def home(request):
    return render(request, 'homepage.html')

def aboutme(request):
    return render(request, 'aboutme.html')

def education(request):
    return render(request, 'education.html')

def experience(request):
    return render(request, 'experience.html')

def portofolio(request):
    return render(request, 'portofolio.html')

def contact(request):
    return render(request, 'contact.html')


